import abc
import logging
import random
from typing import Any

logger = logging.getLogger(__name__)


class Card:
    def __init__(self, value: int, visible=False):
        self._value = value
        self.visible = visible

    def reveal(self):
        self.visible = True

    @property
    def value(self):
        if self.visible:
            return self._value
        else:
            return None

    def __repr__(self):
        return f'Card({self._value}, {"visible" if self.visible else "hidden"})'


class DrawPile:
    def __init__(self):
        cards = []
        for _ in range(5):
            cards.append(Card(-2))
        for _ in range(15):
            cards.append(Card(0))
        for _ in range(10):
            cards.append(Card(-1))
            for i in range(1, 13):
                cards.append(Card(i))
        random.shuffle(cards)

        self._cards = cards

    def draw(self, n=1):
        logging.debug(f'Drawing {n} card{"s" if n > 1 else ""}; {len(self._cards)} remaining')
        if n == 1:
            return self._cards.pop()
        else:
            return [self._cards.pop() for _ in range(n)]


class DiscardPile:
    def __init__(self):
        self._card = None

    def discard(self, card: Card):
        card.visible = True
        logging.debug(f'Discarded card: {card.value}')
        self._card = card

    def draw(self) -> Card:
        assert self._card is not None, "Tried to draw second card from discard pile"

        card = self._card
        self._card = None

        logging.debug(f'Drawing card from discard pile')

        return card

    def peek(self) -> Card:
        assert self._card is not None, "Tried to peek at second card on discard pile"
        return self._card


class CardGrid:
    def __init__(self, draw_pile: DrawPile):
        cards = draw_pile.draw(12)
        self._cards = [cards[0:4], cards[4:8], cards[8:12]]

    def __str__(self):
        def print_card(card):
            if card is None:
                return '  '
            elif card.visible:
                return f'{card.value: >2}'
            else:
                return ' #'
        s = ''
        for y in range(3):
            s += ' '.join([print_card(self._cards[y][x]) for x in range(4)])
            if y < 2:
                s += '\n'
        return s

    def sum(self, visible_only=True):
        s = 0
        for y in range(3):
            for x in range(4):
                card = self._cards[y][x]
                if card is not None:
                    if not visible_only or card.visible:
                        s += card._value
        return s

    def fully_visible(self):
        for y in range(3):
            for x in range(4):
                card = self._cards[y][x]
                if card is not None:
                    if not card.visible:
                        return False
        return True

    def show_all(self):
        for y in range(3):
            for x in range(4):
                card = self._cards[y][x]
                if card is not None:
                    card.visible = True

    def eliminate_columns(self, discard_pile: DiscardPile):
        for x in range(4):
            eliminate = True
            col_val = None
            for y in range(3):
                card = self._cards[y][x]
                # Column already eliminated previously?
                if card is None:
                    eliminate = False
                    break
                # Card still hidden?
                if not card.visible:
                    eliminate = False
                    break
                # First row?
                if y == 0:
                    col_val = card.value
                else:
                    if card.value != col_val:
                        eliminate = False
                        break

            if eliminate:
                for y in range(3):
                    card = self._cards[y][x]
                    self._cards[y][x] = None
                    discard_pile.discard(card)
                    logging.debug(f'Eliminated column of {card.value}')


class Player:
    def __init__(self):
        self.grid: CardGrid = None
        self.score = 0

    def create_grid(self, draw_pile: DrawPile):
        self.grid = CardGrid(draw_pile)


class Agent:
    def __init__(self, player: Player, other_players: [Player], game: 'Game', options=None):
        self.player = player
        self.other_players = other_players
        self.game = game
        self.options = options

    def draw_from_draw_pile(self) -> Card:
        drawn_card = self.game.draw_pile.draw()
        drawn_card.visible = True
        return drawn_card

    def discard_pile_value(self) -> int:
        return self.game.discard_pile.peek().value

    def draw_from_discard_pile(self):
        drawn_card = self.game.discard_pile.draw()
        return drawn_card

    def discard(self, card: Card):
        self.game.discard_pile.discard(card)

    def get_card(self, y, x) -> Card:
        return self.player.grid._cards[y][x]

    def replace_card(self, y, x, new_card):
        old_card = self.player.grid._cards[y][x]
        self.player.grid._cards[y][x] = new_card
        self.discard(old_card)

    @abc.abstractmethod
    def prepare(self):
        pass

    @abc.abstractmethod
    def play(self):
        pass


def wrapping_slice(l, start, end):
    return l[start:] + l[:end]


class Game:
    def __init__(self, agent_classes: list[tuple[type[Agent], Any]]):
        self.draw_pile = DrawPile()
        self.discard_pile = DiscardPile()

        self.players = [Player() for _ in range(len(agent_classes))]

        self.agents = [
            agent_class(self.players[i], wrapping_slice(self.players, i + 1, i), self, options)
            for i, (agent_class, options) in enumerate(agent_classes)
        ]

    def run(self):
        max_sum = float('-inf')
        start_player = 0
        for i, agent in enumerate(self.agents):
            logging.debug(f'Player {i}:')
            agent.player.create_grid(self.draw_pile)
            agent.prepare()
            logging.debug(agent.player.grid)
            s = agent.player.grid.sum()
            if s > max_sum:
                max_sum = s
                start_player = i
        logging.debug(f'Player {start_player} will start')

        round_index = 0
        while True:
            logging.debug(f'Round {round_index} starts!')

            self.discard_pile.discard(self.draw_pile.draw())

            current_player = start_player
            ending = False
            finisher_index = None
            while True:
                logging.debug(f'Player {current_player}\'s turn')
                agent = self.agents[current_player]
                agent.play()
                agent.player.grid.eliminate_columns(self.discard_pile)

                logging.debug(agent.player.grid)

                if not ending and agent.player.grid.fully_visible():
                    finisher_index = current_player
                    ending = True
                    logging.debug(f'Player {finisher_index} ends the round')

                current_player += 1
                if current_player == len(self.players):
                    current_player = 0
                if ending and current_player == finisher_index:
                    break

            # Make all cards visible and possibly eliminate columns
            for player in self.players:
                player.grid.show_all()
                player.grid.eliminate_columns(self.discard_pile)
            # Sum up all cards
            points = [player.grid.sum(visible_only=False) for player in self.players]
            # Check if player who ended the round has the lowest points
            finisher_has_lowest = True
            for i, player in enumerate(self.players):
                if i != finisher_index:
                    if points[i] <= points[finisher_index]:
                        finisher_has_lowest = False
                        break
            # Double points if finisher does not have the lowest
            if not finisher_has_lowest and points[finisher_index] > 0:
                logging.debug(f'Player {finisher_index}\'s points will be doubled')
                points[finisher_index] *= 2
            # Add to score and end game if a player has reached 100
            game_end = False
            for i, player in enumerate(self.players):
                player.score += points[i]
                if player.score >= 100:
                    game_end = True
            logging.debug('Scores:')
            for i, player in enumerate(self.players):
                logging.debug(f'Player {i}: {player.score}')
            if game_end:
                break

            # New round
            round_index += 1
            self.draw_pile = DrawPile()
            self.discard_pile = DiscardPile()
            for agent in self.agents:
                agent.player.create_grid(self.draw_pile)
                agent.prepare()
            start_player = finisher_index

        min_score = float('inf')
        winners = []
        for i, player in enumerate(self.players):
            if player.score < min_score:
                winners = [i]
                min_score = player.score
            elif player.score == min_score:
                winners.append(i)
        if len(winners) > 1:
            logging.debug(f'Players {winners} draw!')
        else:
            logging.debug(f'Player {winners[0]} wins!')

        return list(map(lambda player: player.score, self.players))
