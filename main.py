import tournament
from agents.DumbAgent import DumbAgent
from agents.LessDumbAgent import LessDumbAgent
from agents.SmartAgent import SmartAgent

tournament.rank({
    # 'Dumb': (DumbAgent, None),
    # 'LessDumb5': (LessDumbAgent, 5),
    # 'LessDumb4': (LessDumbAgent, 4),
    'SmartAgentDefault': (SmartAgent, SmartAgent.Options()),
    'SmartAgent1': (SmartAgent, SmartAgent.Options(much_better_thresh=1)),
    'SmartAgent3': (SmartAgent, SmartAgent.Options(much_better_thresh=3)),
}, players=5, games=10000)
