import random

import skyjo


class DumbAgent(skyjo.Agent):
    def prepare(self):
        # Always reveal the same cards
        self.get_card(0, 0).reveal()
        self.get_card(0, 1).reveal()

    def play(self):
        # Always draw from draw pile
        drawn_card = self.draw_from_draw_pile()
        # Replace a random hidden card (loops until hidden card found)
        while True:
            y = random.randint(0, 2)
            x = random.randint(0, 3)
            card = self.get_card(y, x)
            if card is not None and not card.visible:
                self.replace_card(y, x, drawn_card)
                break
