import random
from dataclasses import dataclass

import skyjo


# TODO:
#  - only draw from discard pile if it improves score
#  - always replace highest card (unless eliminateable?)
#  - keep track of eliminateable columns? abandon if other players close to finishing
#  - delay last card until sure to finish best (might need to remove dumb agents who will always finish quickly)


class SmartAgent(skyjo.Agent):
    @dataclass
    class Options:
        much_better_thresh: int = 2; """ What difference in card value is "much better" """

    def prepare(self):
        self.get_card(0, 0).reveal()
        self.get_card(0, 1).reveal()

    def play(self):
        # Inspect our cards
        # cards = []
        # hidden = []
        hidden_coord = None
        value_max = -3
        coords_max = None
        n_hidden = 0
        for y in range(3):
            for x in range(4):
                card = self.get_card(y, x)
                if card is not None:
                    if card.visible:
                        v = card.value
                        if v > value_max:
                            value_max = v
                            coords_max = (y, x)
                        #cards.append((y, x, card))
                    else:
                        n_hidden += 1
                        # hidden.append((y, x))
                        if hidden_coord is None:
                            hidden_coord = (y, x)

        discard_value = self.discard_pile_value()
        # Special case to not end the round too early
        if n_hidden == 1:
            # TODO: check other player's points
            # hidden_coord = hidden[0]
            if value_max - discard_value >= self.options.much_better_thresh:
                # If the discarded card is much better than the currently worst, use it to replace the worst card
                drawn_card = self.draw_from_discard_pile()
                self.replace_card(coords_max[0], coords_max[1], drawn_card)
            elif discard_value <= 5:
                # If the discarded card is good, use it to replace the last hidden card (ending the round)
                drawn_card = self.draw_from_discard_pile()
                self.replace_card(hidden_coord[0], hidden_coord[1], drawn_card)
            else:
                # Otherwise, draw from the draw pile
                drawn_card = self.draw_from_draw_pile()
                if value_max - drawn_card.value >= self.options.much_better_thresh:
                    # If it's much better than the currently worst, use it to replace the worst card
                    self.replace_card(coords_max[0], coords_max[1], drawn_card)
                elif drawn_card.value <= 5:
                    # If it's better than average, replace the hidden card (ending the round)
                    self.replace_card(hidden_coord[0], hidden_coord[1], drawn_card)
                else:
                    # If it's worse than average, discard it and turn over the hidden card instead (ending the round)
                    self.get_card(hidden_coord[0], hidden_coord[1]).reveal()
                    self.discard(drawn_card)
        else:
            if discard_value <= 5:
                # Draw from discard pile if it's better than average
                drawn_card = self.draw_from_discard_pile()
                if value_max > discard_value:
                    # If possible, improve the visible cards
                    self.replace_card(coords_max[0], coords_max[1], drawn_card)
                else:
                    self.replace_card(hidden_coord[0], hidden_coord[1], drawn_card)
            else:
                drawn_card = self.draw_from_draw_pile()
                if drawn_card.value <= 5:
                    # Use a good card to replace the worst card
                    if value_max > drawn_card.value:
                        self.replace_card(coords_max[0], coords_max[1], drawn_card)
                    else:
                        # Try to improve a still hidden card
                        self.replace_card(hidden_coord[0], hidden_coord[1], drawn_card)
                elif value_max - drawn_card.value >= self.options.much_better_thresh:
                    # If the worst card is very bad, replace it
                    self.replace_card(coords_max[0], coords_max[1], drawn_card)
                else:
                    # If the drawn card wouldn't improve our situation by much, discard it
                    # hidden_coord = hidden[0]
                    self.get_card(hidden_coord[0], hidden_coord[1]).reveal()
                    self.discard(drawn_card)

