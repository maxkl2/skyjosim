import random
import time
from dataclasses import dataclass
from typing import Type, Any

import skyjo


@dataclass
class Result:
    rounds: int = 0
    score: int = 0
    wins: int = 0


def rank(configs: dict[str, tuple[Type[skyjo.Agent], Any]], players: int, games: int):
    t_start = time.perf_counter()

    config_tuples = list(configs.items())
    results = {k: Result() for k in configs.keys()}
    for _ in range(games):
        round_configs = random.choices(config_tuples, k=players)
        agents = list(map(lambda config: config[1], round_configs))
        game = skyjo.Game(agents)

        scores = game.run()

        winner = 0
        winner_score = scores[0]
        for i, score in enumerate(scores):
            config_name = round_configs[i][0]
            results[config_name].rounds += 1
            results[config_name].score += score
            if score < winner_score:
                winner_score = score
                winner = i
        config_name = round_configs[winner][0]
        results[config_name].wins += 1

    t_end = time.perf_counter()

    print(f'Ran {games} games in {t_end - t_start:.2f} s ({games / (t_end - t_start):.1f} games/s)')

    scores_avg = {name: result.score / result.rounds for name, result in results.items()}
    wins_percent = {name: result.wins / result.rounds * 100 for name, result in results.items()}

    print('Results:')
    for name, result in results.items():
        print(f' {name}: {result.score} points ({scores_avg[name]:.1f} avg), {result.wins} wins ({wins_percent[name]:.1f} %) in {result.rounds} rounds')

    name_score = list(scores_avg.items())
    name_score.sort(key=lambda t: t[1])
    print('Ranking by score:', ', '.join(map(lambda t: f'{t[0]} ({t[1]:.1f})', name_score)))

    name_wins = list(wins_percent.items())
    name_wins.sort(key=lambda t: t[1], reverse=True)
    print('Ranking by wins:', ', '.join(map(lambda t: f'{t[0]} ({t[1]:.1f})', name_wins)))